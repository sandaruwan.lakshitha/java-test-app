package ca.randoli.quickstart.springboot.helloservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootTest
class HelloServiceApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(HelloServiceApplicationTests.class);
	@Test
	void contextLoads() {
        logger.info("this is a info message");
	}


}
